% This function performs a simulation of information diffusion
% through a network using a BASS agent-based model. It takes as its
% input a set of directed edges connecting an agent referred to as an
% influencer to an agent referred to an influencee. The function
% outputs a count of aware agents at each time step.
%
% Let E denote the number of directed edges in the social network. 
%
% Input: 
%        edges_matrix = E x 2 matrix
%        with entries (i,j) that indicate that there is a 
%        directed edge from agent i to agent j.
%
%        times = a vector of times starting with 0 given in hours
%        at which each time step occurs
%
%        p = advertising parameter.
%        q = word-of-mouth parameter.
%        
% Output:
%        number_aware = a vector that records the
%        number of agents that are aware at each timestep.
%        
% Written by Neza Vodopivec in October 2012.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function number_aware = bass(edge_matrix,times,p,q)

times = times(2:end);
number_of_timesteps = length(times);

%  Delta t is equivalent to the first time after 0.
p = p*times(1);
q = q*times(1);

% Arbitrarily label each agent from 1...N.
[agents,~,edges] = unique(edge_matrix(:));
edges = reshape(edges,[],2);

% Create vectors recording the following for each agent at the current 
% timestep:
%   - its number of influencees,
%   - its number of influencers,
%   - its number of aware influencers.

number_of_agents = length(agents);
number_of_influencees = histc(edges(:,1),1:number_of_agents);
number_of_influencers = histc(edges(:,2),1:number_of_agents);
number_of_influencers_aware = zeros(number_of_agents,1);

agents_aware      = zeros(number_of_agents,1);
% Optional output possibly useful for later purposes.
time_of_awareness = zeros(number_of_agents,1);

number_aware = zeros(number_of_timesteps+1,1);

% Create an 'influencee vector', a stacked list containing the influencees 
% corresponding to each influencer. Keep track of which entries of the 
% influencee vector correspond to each influencer.
influencee_vector = sortrows(edges);
influencee_vector = influencee_vector(:,2);

% Create list of coordinates indicating the first and last entries of influencee 
% vector that corresponds to each influencer.
influencer_coords = cumsum([0; number_of_influencees]);
influencer_coords = [influencer_coords(1:end-1)+1 influencer_coords(2:end)];

% Repeat at each timestep:

for i = 1:number_of_timesteps

    % Compute the current fraction of aware influencers for each agent.
    fraction_of_influencers_aware = number_of_influencers_aware./(number_of_influencers + ~number_of_influencers);
    % Determine newly aware agents.
    agents_aware_due_to_wom = rand(number_of_agents,1) <= q*fraction_of_influencers_aware;
    agents_aware_due_to_ads = rand(number_of_agents,1) <= p;
    % Identify the indices of newly aware agents.
    newly_aware_index = find((agents_aware_due_to_ads | agents_aware_due_to_wom) & ~agents_aware);
   
    % Increment the number of aware influencers for each agent.
    for k = 1:length(newly_aware_index)
        j = influencee_vector(influencer_coords(newly_aware_index(k),1):influencer_coords(newly_aware_index(k),2));
        number_of_influencers_aware(j) = number_of_influencers_aware(j) + 1;
    end
    
    % Label newly-aware agents.
    agents_aware(newly_aware_index) = 1;
    % Keep track of the timestep at which each agent becomes aware.
    % time_of_awareness(newly_aware_index) = time(i);
    % Record cummulative number of aware agents.
    number_aware(i+1) = sum(agents_aware);

end