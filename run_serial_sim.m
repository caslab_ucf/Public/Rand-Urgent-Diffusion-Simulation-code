% Returns vector with average aware at each time step.
% WARNING: number_of_runs must be at least 2.

function average_aware = run_serial_sim(edges,times, p,q,number_of_runs,option)

number_aware = zeros(number_of_runs,length(times));
if strcmp(option,'bass')   
    for i=1:number_of_runs
        number_aware(i,:) = bass(edges,times,p,q);
    end
end

 if strcmp(option,'cascade')  
    for i=1:number_of_runs
        number_aware(i,:) = cascade(edges,times,p,q);
    end
 end
 
 if strcmp(option,'threshold')  
    for i=1:number_of_runs, 
        number_aware(i,:) = threshold(edges,times,p,q);
    end
 end
  
average_aware = mean(number_aware,1);
