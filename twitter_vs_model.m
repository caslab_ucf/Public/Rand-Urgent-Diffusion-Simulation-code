% This function  creates a plot of Twitter data vs. agent-based Bass model or 
% agent-based cascade adaptation model.
%
% Let E denote the number of directed edges in the social network. 
%
% Input: 
%
%        twitter_times = observed times starting with 0 given in secods
%        at which each time step occurs. 
%
%        edges = E x 2 matrix
%        with entries (i,j) that indicate that there is a 
%        directed edge from agent i to agent j.
%
%        number_of_runs = number of times simulation should be run.
%           *MUST BE AT LEAST 2*
%
%        delta_t = length of time step (in hours).   
%
%        p = advertising parameter.
%        q = word-of-mouth parameter.
%                   
%        option = string giving which implementation should be run
%        use 'bass' or 'cascade'
%
% Output:
%        true_aware = total number of observed users aware at time t
%
%        sim_aware = mean number of agents aware at time t
%        (over given number of simulations).
%        
% Written by Neza Vodopivec in November 2012.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [true_aware, sim_aware] = twitter_vs_model(twitter_times,edges,number_of_runs,delta_t,p,q,option) 

[true_aware,times] = bin_by_time (twitter_times,delta_t);

sim_aware = run_serial_sim(edges,times,p,q,number_of_runs,option);

% Plot

plot(times,true_aware,'b', 'LineWidth',2);
hold on;
plot(times,sim_aware,'r','LineWidth',2);

axis tight;
xlabel('Time (Hours)');
ylabel('Number of Aware Agents');
legend('twitter times',option,'Location','East');
hold off;