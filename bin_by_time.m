% This function computes the cummulative number of true Twitter 
% users who are aware at each time step.  It takes times individual users
% posted messages in seconds starting with t = 0 as input.
%
% Written by Neza Vodopivec in December 2012.

function [number_aware,times] = bin_by_time (twitter_times,delta_t)

% Convert times from seconds to hours.
twitter_in_hours = (twitter_times)/3600;
number_of_hours = ceil(max(twitter_in_hours));
times = 0:delta_t:number_of_hours;

% Compute total number of users aware at each time.
binned_times = histc(twitter_in_hours,times);
number_aware = [0; cumsum(binned_times(1:end-1))];
plot(times,number_aware,'LineWidth',2);